Welcome bootcampers,

this is the first project you will encounter. In this GIT repository, there is a project you will work on. There is nothing implemented in the project but basic setup needed for compilation and run.



As a first assignment, complete following tasks:

- use GIT to checkout this project to your computer

- change remote repository to your assigned one

- commit & push project to your repository

- compile and run project locally via maven and your IDE:

    - you must install oracle driver locally first, process following command in (project)/libs folder:

	mvn install:install-file -Dfile=ojdbc7.jar -DgroupId=com.oracle -DartifactId=ojdbc7 -Dversion=12.1.0.2 -Dpackaging=jar


You might also need a proxy setup. Please issue following command to setup a proxy for GIT (usr and pwd will be provided to you):


Congratulations. You are now prepared to do some real coding. Be aware, that only changes made in your repository will count.
We are unable to test code which isn't in your assigned repository.