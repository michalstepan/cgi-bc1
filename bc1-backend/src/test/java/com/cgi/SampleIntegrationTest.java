package com.cgi;

import com.cgi.controller.SampleRestController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author stepanm
 */
// integration test setup. For more information, see https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-testing.html
@RunWith(SpringRunner.class)
// integration test is based on controller class
@WebMvcTest(SampleRestController.class)
public class SampleIntegrationTest {

    // MockMvc enables you to test application endpoints
    @Autowired
    private MockMvc mvc;

    // sample integration test
    @Test
    public void testMethod() throws Exception {
        this.mvc.perform(get("/test").accept(MediaType.TEXT_PLAIN))
                .andExpect(status().isOk()).andExpect(content().string("Sample rest webservice"));
    }
}