package com.cgi;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

// unit test setup. For more information, see https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-testing.html
@RunWith(SpringRunner.class)
@SpringBootTest
public class SampleUnitTest {

    // sample unit test
    @Test
    public void sampleTest() {
        boolean isTrue = true;
        Assert.assertTrue(isTrue);
    }
}
