package com.cgi;

import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
// This import statement runs configuration for attached class
@Import(AppConfig.class)
public class ApplicationRunner {

    // starting method of whole application, as you should already know
    public static void main(String[] args) {

        // next statement runs spring web server, which allows you to use controllers, services, datasource atc.
        // for more information, how this works, see: https://spring.io/guides/gs/spring-boot/
        SpringApplication.run(ApplicationRunner.class, args);
    }
}
