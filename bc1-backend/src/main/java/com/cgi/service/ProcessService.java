package com.cgi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author stepanm
 */
// service annotation tells spring that this is bean and have some bussiness logic
@Service
public class ProcessService {

    // autowired annotation enables you following object to be used without prior instatialization with new. Only beans could be autowired

    // DataSource is bean which is specified in AppConfig.class. It enables you working with database. DataSource must be properly configured.
    @Autowired
    private DataSource dataSource;

    // Annotation Scheduled on method runs following method in specified frequency.
    @Scheduled(fixedRate = 5000)
    public void parse() {
        simpleDatabaseCall();
    }

    // sample database call using dataSource
    private void simpleDatabaseCall() {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from Person");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String name = resultSet.getString(1);
                String surname = resultSet.getString(2);
                System.out.println(String.format("[connection] Database resident: %s %s", name, surname));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
