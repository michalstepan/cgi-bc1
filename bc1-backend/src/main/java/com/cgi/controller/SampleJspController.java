package com.cgi.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author stepanm
 */
// controller annotation enables you to call specified method from other sources, f.e. web browser
// for more informations on controllers and MVC, see official documentation - https://docs.spring.io/spring/docs/current/spring-framework-reference/html/mvc.html
@Controller
public class SampleJspController {

    // this sample call says that once you call /jsp endpoint, jsp file with name "sample.jsp" in webapp root folder will be populated
    @RequestMapping(value = "/jsp", method = RequestMethod.GET)
    public String testMethodJsp() {
        return "sample";
    }
}
