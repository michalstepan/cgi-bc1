package com.cgi.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author stepanm
 */
// rest controller annotation enables you to call specified method from other sources, f.e. web browser
// difference between RestController and Controller is that RestController automatically provides serialized (json) form, but Controller lookup for jsp pages.
@RestController
public class SampleRestController {

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String testMethod() {
        return "Sample rest webservice";
    }
}
