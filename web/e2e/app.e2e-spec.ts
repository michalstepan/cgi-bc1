import { JbootcampPage } from './app.po';

describe('jbootcamp App', () => {
  let page: JbootcampPage;

  beforeEach(() => {
    page = new JbootcampPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
