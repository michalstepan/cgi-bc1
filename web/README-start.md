## Postup pro zprovoznění webu
0. Doporucuji si nainstalovat Visual studio code (konzoli lze otevrit pomoci CTRL+tilda)
    - pro instalaci rozšíření na firemní síti je třeba si nastavit proxy http://proxy.logica.com:80 v nastavení VS code
1. Nainstalovat nodejs
    - verzi obsahující npm 3
    - nastavení proxy: `npm set proxy http://proxy.logica.com/`
2. Nainstalovat angular CLI globalne: `npm install -g @angular/cli` (je treba mit nodejs v PATH). 
    - Wiki angular CLI https://github.com/angular/angular-cli/wiki
3. Ve slozce s webovou aplikaci napr. C:\projects\jbootcamp\trunk\Web\ spustit `npm install`

Takto jsme si pripravili projekt do spustitelne podoby stazenim zavislosti.

## Lokalni spusteni
Pro spusteni lokalne pouzijeme `ng serve`, coz spusti kompilaci a server, ktery automaticky detekuje zmeny, nejspíše na http://localhost:4200

## Release build
Pro spusteni kompilace pro release spustime `ng build -prod`, ktery pripravi web do slozky dist.