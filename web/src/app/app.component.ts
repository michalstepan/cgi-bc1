import { Component } from '@angular/core';

import { ExampleService } from './example.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private exampleService: ExampleService) { }

  title = 'app works!';

  loadData() {
    this.exampleService.getExaple().subscribe(
      result => this.title = result + '!!!',
      error => console.error(error)
    );
  }
}
