import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx'; // inmport operatoru jako je map

@Injectable()
export class ExampleService {

  constructor(private http: Http) { }

  getExaple(): Observable<any> {
    return this.http.get('http://localhost:8080/test').share().map(x=>x.text());
  }
}
